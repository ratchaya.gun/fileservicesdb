import db from '~/models'
import pathConfig from '~/config/path'

const Food = db.Food
const Ingrediant = db.Ingrediant
const Op = db.Sequelize.Op

const create = async (req, res) => {
  const data = req.body
  try {
    const foundFood = await Food.findOne({
      where: {
        date: data.date,
        userId: data.userId
      }
    })
    let resFood = null
    if (foundFood) {
      resFood = await foundFood.update(data)
    }
    else {
      resFood = await Food.create(data)
    }
    res.send({
      response_status: 'SUCCESS',
      data: {
        ...resFood.dataValues,
      },
    })
  }
  catch (err) {
    res.send({
      response_status: 'ERROR',
      message: err.message || 'Some error occurred while creating the food.',
    })
  }
}

const findAll = async (req, res) => {
  try {
    let foods = []
    let lang = 'th'
    if (Object.keys(req.query).length > 0) {
      const where = {}
      Object.keys(req.query).forEach(key => {
        if (key === 'lang') {
          lang = req.query[key]
          return
        }
        where[key] = req.query[key]
      })
      foods = await Food.findAll({
        order: [
          [ 'createdAt', 'DESC' ],
        ],
        where,
        include: Ingrediant,
      })
    }
    else {
      foods = await Food.findAll({
        order: [
          [ 'createdAt', 'DESC' ],
        ],
        include: Ingrediant,
      })
    }
    foods = foods.map((food) => {
      return {
        ...food.dataValues,
        imgPath: `${food.dataValues.imgPath}`
      }
    })
    res.send({
      response_status: 'SUCCESS',
      data: foods,
    })
  }
  catch (err) {
    res.send({
      response_status: 'ERROR',
      message: err.message || 'Some error occurred while retrieving the foods.',
    })
  }
}

// const findOne = async (req, res) => {
//   try {
//     const id = req.params.id
//     const food = await Food.findOne({ where: { id }})
//     res.send({
//       response_status: 'SUCCESS',
//       data: food.dataValues,
//     })
//   }
//   catch (err) {
//     res.send({
//       response_status: 'ERROR',
//       message: err.message || 'Some error occurred while retrieving the food.',
//     })
//   }
// }

// const update = async (req, res) => {
//   try {
//     const data = req.body
//     const id = req.params.id
//     const updatedFood = {
//       name: data.name,
//       price: data.price,
//       food: data.food,
//       type: data.type,
//     }
//     let food = 0
//     if (data.imgUrl) {
//       let base64Image = data.imgUrl.split(';base64,').pop()
//       const imageBitMap = new Buffer(base64Image, 'base64')
//       let mime = await fileType.fromBuffer(imageBitMap)
//       const filePath = `public/food-images/food-${id}.${mime.ext}`
//       fs.writeFileSync(filePath, imageBitMap)

//       food = await Food.update({
//         ...updatedFood,
//         imgUrl: `https://bansaiservice.mediacreativecenter3.com/${filePath.replace('public/', '')}`,
//       },
//       {
//         where: { id }
//       })
//     }
//     else {
//       food = await Food.update(updatedFood, { where: { id }})
//     }
//     if (food == 1) {
//       res.send({
//         response_status: 'SUCCESS',
//         message: 'Food was updated successfully',
//       })
//     }
//     else {
//       res.send({
//         response_status: 'ERROR',
//         message: `Food (ID: ${id}) was not found.`,
//       })
//     }
//   }
//   catch (err) {
//     res.send({
//       response_status: 'ERROR',
//       message: err.message || 'Some error occurred while updating the food.',
//     })
//   }
// }

// const remove = async (req, res) => {
//   try {
//     const id = req.params.id
//     const food = await Food.destroy({ where: { id }})
//     if (food == 1) {
//       res.send({
//         response_status: 'SUCCESS',
//         message: 'Food was deleted successfully',
//       })
//     }
//     else {
//       res.send({
//         response_status: 'ERROR',
//         message: `Log (ID: ${id}) was not found.`,
//       })
//     }
//   }
//   catch (err) {
//     res.send({
//       response_status: 'ERROR',
//       message: err.message || 'Some error occurred while deleting the food.',
//     })
//   }
// }

export default {
  // create,
  findAll,
  // findOne,
  // update,
  // remove,
  // findAllWithTypes,
}