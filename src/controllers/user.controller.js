import db from '~/models'
import bcrypt from 'bcrypt'

const User = db.User
const Op = db.Sequelize.Op

const create = async(req, res) => {
    try {
        const user = req.body
        const resUser = await User.create(user)
        res.send({
            response_status: 'SUCCESS',
            data: resUser.dataValues,
        })
    } catch (err) {
        res.send({
            response_status: 'ERROR',
            message: err.message || 'Some error occurred while creating the user.',
        })
    }
}

const login = async(req, res) => {
    const { username, password } = req.body
    try {
        const foundUser = await User.findOne({ where: { username } })

        if (foundUser) {
            console.log(password);
            console.log(foundUser.password)
            if (!bcrypt.compareSync(password, foundUser.password)) {
                res.send({
                    response_status: 'ERROR',
                    message: 'Your password is wrong.'
                })
            } else {
                delete foundUser.dataValues.password
                res.send({
                    response_status: 'SUCCESS',
                    data: foundUser,
                })
            }
        } else {
            res.send({
                response_status: 'ERROR',
                message: `${username} is not found on system.`
            })
        }
    } catch (err) {
        res.send({
            response_status: 'ERROR',
            message: err.message || 'Some error occurred while logging in.',
        })
    }
}

const loginWithSocial = async(req, res) => {
    const { socialId } = req.body
    console.log(socialId)
    try {
        const foundUser = await User.findOne({ where: { socialId } })
        if (foundUser) {
            delete foundUser.dataValues.password
            res.send({
                response_status: 'SUCCESS',
                data: {
                    user: foundUser.dataValues,
                },
            })
        } else {
            const resUser = await User.create({
                socialId,
                username: socialId,
                password: socialId,
            })
            console.log(resUser)
            delete resUser.dataValues.password
            res.send({
                response_status: 'SUCCESS',
                data: resUser.dataValues,
            })
        }
    } catch (err) {
        res.send({
            response_status: 'ERROR',
            message: err.message || 'Some error occurred while logging in.',
        })
    }
}

const findAll = async(req, res) => {
    try {
        if (req.query.type) {
            const users = await User.findAll({
                where: { type: req.query.type },
                order: [
                    ['createdAt', 'DESC'],
                ]
            })
            res.send({
                response_status: 'SUCCESS',
                data: users,
            })
            return
        }
        const users = await User.findAll({
            order: [
                ['createdAt', 'DESC'],
            ]
        })
        res.send({
            response_status: 'SUCCESS',
            data: users,
        })
    } catch (err) {
        res.send({
            response_status: 'ERROR',
            message: err.message || 'Some error occurred while retrieving the users.',
        })
    }
}

const findOne = async(req, res) => {
    try {
        const id = req.params.id
        const user = await User.findOne({ where: { id } })
        res.send({
            response_status: 'ERROR',
            data: user.dataValues,
        })
    } catch (err) {
        res.send({
            response_status: 'ERROR',
            message: err.message || 'Some error occurred while retrieving the user.',
        })
    }
}

const update = async(req, res) => {
    try {
        const id = req.params.id
        const user = await User.update(req.body, { where: { id } })
        if (user == 1) {
            res.send({
                response_status: 'SUCCESS',
                message: 'User was updated successfully',
            })
        } else {
            res.send({
                response_status: 'ERROR',
                message: `User (ID: ${id}) was not found.`,
            })
        }
    } catch (err) {
        res.send({
            response_status: 'ERROR',
            message: err.message || 'Some error occurred while updating the user.',
        })
    }
}

const approve = async(req, res) => {
    try {
        const id = req.params.id
        const user = await User.update({ activeFlag: true }, { where: { id } })
        if (user == 1) {
            res.send({
                response_status: 'SUCCESS',
                message: 'User was approved successfully',
            })
        } else {
            res.send({
                response_status: 'ERROR',
                message: `User (ID: ${id}) was not found.`,
            })
        }
    } catch (err) {
        res.send({
            response_status: 'ERROR',
            message: err.message || 'Some error occurred while updating the user.',
        })
    }
}

const remove = async(req, res) => {
    try {
        const id = req.params.id
        const user = await User.destroy({ where: { id } })
        if (user == 1) {
            res.send({
                response_status: 'SUCCESS',
                message: 'User was deleted successfully',
            })
        } else {
            res.send({
                response_status: 'ERROR',
                message: `Log (ID: ${id}) was not found.`,
            })
        }
    } catch (err) {
        res.send({
            response_status: 'ERROR',
            message: err.message || 'Some error occurred while deleting the user.',
        })
    }
}

export default {
    create,
    login,
    findAll,
    update,
    findOne,
    remove,
    approve,
    loginWithSocial
}