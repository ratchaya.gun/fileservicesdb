import db from '~/models'

const Store = db.Store
const Op = db.Sequelize.Op

const create = async (req, res) => {
  const data = req.body
  try {
    // const foundStore = await Store.findOne({
    //   where: {
    //     date: data.date,
    //   }
    // })
    let resStore = null
    resStore = await Store.create(data)
    // if (foundStore) {
    //   resStore = await foundStore.update(data)
    // }
    // else {
    // }
    res.send({
      response_status: 'SUCCESS',
      data: {
        ...resStore.dataValues,
      },
    })
  }
  catch (err) {
    res.send({
      response_status: 'ERROR',
      message: err.message || 'Some error occurred while creating the store.',
    })
  }
}

const findAll = async (req, res) => {
  try {
    let stores = []
    let lang = 'th'
    if (Object.keys(req.query).length > 0) {
      const where = {}
      Object.keys(req.query).forEach(key => {
        if (key === 'lang') {
          lang = req.query[key]
          return
        }
        where[key] = req.query[key]
      })
      stores = await Store.findAll({
        order: [
          [ 'createdAt', 'DESC' ],
        ],
        where,
        include: [ 'food' ],
      })
    }
    else {
      stores = await Store.findAll({
        order: [
          [ 'createdAt', 'DESC' ],
        ],
        include: [ 'food' ],
      })
    }
    res.send({
      response_status: 'SUCCESS',
      data: stores,
    })
  }
  catch (err) {
    res.send({
      response_status: 'ERROR',
      message: err.message || 'Some error occurred while retrieving the stores.',
    })
  }
}

// const findOne = async (req, res) => {
//   try {
//     const id = req.params.id
//     const store = await Store.findOne({ where: { id }})
//     res.send({
//       response_status: 'SUCCESS',
//       data: store.dataValues,
//     })
//   }
//   catch (err) {
//     res.send({
//       response_status: 'ERROR',
//       message: err.message || 'Some error occurred while retrieving the store.',
//     })
//   }
// }

// const update = async (req, res) => {
//   try {
//     const data = req.body
//     const id = req.params.id
//     const updatedStore = {
//       name: data.name,
//       price: data.price,
//       store: data.store,
//       type: data.type,
//     }
//     let store = 0
//     if (data.imgUrl) {
//       let base64Image = data.imgUrl.split(';base64,').pop()
//       const imageBitMap = new Buffer(base64Image, 'base64')
//       let mime = await fileType.fromBuffer(imageBitMap)
//       const filePath = `public/store-images/store-${id}.${mime.ext}`
//       fs.writeFileSync(filePath, imageBitMap)

//       store = await Store.update({
//         ...updatedStore,
//         imgUrl: `https://bansaiservice.mediacreativecenter3.com/${filePath.replace('public/', '')}`,
//       },
//       {
//         where: { id }
//       })
//     }
//     else {
//       store = await Store.update(updatedStore, { where: { id }})
//     }
//     if (store == 1) {
//       res.send({
//         response_status: 'SUCCESS',
//         message: 'Store was updated successfully',
//       })
//     }
//     else {
//       res.send({
//         response_status: 'ERROR',
//         message: `Store (ID: ${id}) was not found.`,
//       })
//     }
//   }
//   catch (err) {
//     res.send({
//       response_status: 'ERROR',
//       message: err.message || 'Some error occurred while updating the store.',
//     })
//   }
// }

// const remove = async (req, res) => {
//   try {
//     const id = req.params.id
//     const store = await Store.destroy({ where: { id }})
//     if (store == 1) {
//       res.send({
//         response_status: 'SUCCESS',
//         message: 'Store was deleted successfully',
//       })
//     }
//     else {
//       res.send({
//         response_status: 'ERROR',
//         message: `Log (ID: ${id}) was not found.`,
//       })
//     }
//   }
//   catch (err) {
//     res.send({
//       response_status: 'ERROR',
//       message: err.message || 'Some error occurred while deleting the store.',
//     })
//   }
// }

export default {
  create,
  findAll,
  // findOne,
  // update,
  // remove,
  // findAllWithTypes,
}