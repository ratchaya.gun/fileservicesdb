import fs from 'fs'
import fileType from 'file-type'
import db from '~/models'
import pathConfig from '~/config/path'

const Meal = db.Meal
const MealFood = db.MealFood
const Food = db.Food
const Ingrediant = db.Ingrediant
const Op = db.Sequelize.Op

const create = async (req, res) => {
  const data = req.body
  try {
    const resMeal = await Meal.create({
      ...data,
      imgPath: '',
    })
    const mealId = resMeal.dataValues.id
    if (data.imgPath) {
      let base64Image = data.imgPath.split(';base64,').pop()
      const imageBitMap = new Buffer(base64Image, 'base64')
      let mime = await fileType.fromBuffer(imageBitMap)
      const filePath = `public/meal-images/meal-${mealId}.${mime.ext}`
      fs.writeFileSync(filePath, imageBitMap)
      await resMeal.update({
        imgPath: `/${filePath.replace('public/', '')}`,
      })
    }
    const resFoods = await Promise.all(data.foods.map(async (food) => {
      return await MealFood.create({
        mealId,
        foodId: food.id,
      })
    }))
    res.send({
      response_status: 'SUCCESS',
      data: {
        ...resMeal.dataValues,
      },
    })
  }
  catch (err) {
    res.send({
      response_status: 'ERROR',
      message: err.message || 'Some error occurred while creating the meal.',
    })
  }
}

const findAll = async (req, res) => {
  try {
    let meals = []
    let lang = 'th'
    if (Object.keys(req.query).length > 0) {
      const where = {}
      Object.keys(req.query).forEach(key => {
        if (key === 'lang') {
          lang = req.query[key]
          return
        }
        where[key] = req.query[key]
      })
      meals = await Meal.findAll({
        order: [
          [ 'createdAt', 'DESC' ],
        ],
        where,
        include: [
          {
            model: MealFood,
            include: {
              model: Food,
              include: {
                model: Ingrediant,
              },
            },
          },
        ],
      })
    }
    else {
      meals = await Meal.findAll({
        order: [
          [ 'createdAt', 'DESC' ],
        ],
        include: [
          {
            model: MealFood,
            include: {
              model: Food,
              include: {
                model: Ingrediant,
              },
            },
          },
        ],
      })
    }
    meals = meals.map((meal) => {
      return {
        ...meal.dataValues,
        imgPath: `${meal.dataValues.imgPath}`,
        mealFoods: meal.dataValues.mealFoods.map((mealFood) => {
          return {
            ...mealFood.dataValues,
            food: { 
              ...mealFood.dataValues.food.dataValues,
              imgPath: `${mealFood.dataValues.food.dataValues.imgPath}`,
            },
          }
        }),
      }
    })
    res.send({
      response_status: 'SUCCESS',
      data: meals,
    })
  }
  catch (err) {
    res.send({
      response_status: 'ERROR',
      message: err.message || 'Some error occurred while retrieving the meals.',
    })
  }
}

// const findOne = async (req, res) => {
//   try {
//     const id = req.params.id
//     const meal = await Meal.findOne({ where: { id }})
//     res.send({
//       response_status: 'SUCCESS',
//       data: meal.dataValues,
//     })
//   }
//   catch (err) {
//     res.send({
//       response_status: 'ERROR',
//       message: err.message || 'Some error occurred while retrieving the meal.',
//     })
//   }
// }

// const update = async (req, res) => {
//   try {
//     const data = req.body
//     const id = req.params.id
//     const updatedMeal = {
//       name: data.name,
//       price: data.price,
//       meal: data.meal,
//       type: data.type,
//     }
//     let meal = 0
//     if (data.imgPath) {
//       let base64Image = data.imgPath.split(';base64,').pop()
//       const imageBitMap = new Buffer(base64Image, 'base64')
//       let mime = await fileType.fromBuffer(imageBitMap)
//       const filePath = `public/meal-images/meal-${id}.${mime.ext}`
//       fs.writeFileSync(filePath, imageBitMap)

//       meal = await Meal.update({
//         ...updatedMeal,
//         imgPath: `https://bansaiservice.mediacreativecenter3.com/${filePath.replace('public/', '')}`,
//       },
//       {
//         where: { id }
//       })
//     }
//     else {
//       meal = await Meal.update(updatedMeal, { where: { id }})
//     }
//     if (meal == 1) {
//       res.send({
//         response_status: 'SUCCESS',
//         message: 'Meal was updated successfully',
//       })
//     }
//     else {
//       res.send({
//         response_status: 'ERROR',
//         message: `Meal (ID: ${id}) was not found.`,
//       })
//     }
//   }
//   catch (err) {
//     res.send({
//       response_status: 'ERROR',
//       message: err.message || 'Some error occurred while updating the meal.',
//     })
//   }
// }

const remove = async (req, res) => {
  try {
    const id = req.params.id
    await MealFood.destroy({ where: { mealId: id }})
    const meal = await Meal.destroy({ where: { id }})
    if (meal == 1) {
      res.send({
        response_status: 'SUCCESS',
        message: 'Meal was deleted successfully',
      })
    }
    else {
      res.send({
        response_status: 'ERROR',
        message: `Log (ID: ${id}) was not found.`,
      })
    }
  }
  catch (err) {
    res.send({
      response_status: 'ERROR',
      message: err.message || 'Some error occurred while deleting the meal.',
    })
  }
}

export default {
  create,
  findAll,
  // findOne,
  // update,
  remove,
  // findAllWithTypes,
}