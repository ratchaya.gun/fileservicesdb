import db from '~/models'

const Professor = db.Professor
const Op = db.Sequelize.Op

const create = async (req, res) => {
  const data = req.body
  try {
    const resProfessor = await Professor.create({
      ...data,
      imgPath: '',
    })
    const professorId = resProfessor.dataValues.id
    if (data.imgPath) {
      let base64Image = data.imgPath.split(';base64,').pop()
      const imageBitMap = new Buffer(base64Image, 'base64')
      let mime = await fileType.fromBuffer(imageBitMap)
      const filePath = `public/professor-images/professor-${professorId}.${mime.ext}`
      fs.writeFileSync(filePath, imageBitMap)
      await resProfessor.update({
        imgPath: `/${filePath.replace('public/', '')}`,
      })
    }
    res.send({
      response_status: 'SUCCESS',
      data: {
        ...resProfessor.dataValues,
      },
    })
  }
  catch (err) {
    res.send({
      response_status: 'ERROR',
      message: err.message || 'Some error occurred while creating the professor.',
    })
  }
}

const findAll = async (req, res) => {
  try {
    let professors = []
    let lang = 'th'
    if (Object.keys(req.query).length > 0) {
      const where = {}
      Object.keys(req.query).forEach(key => {
        if (key === 'lang') {
          lang = req.query[key]
          return
        }
        where[key] = req.query[key]
      })
      professors = await Professor.findAll({
        order: [
          [ 'createdAt', 'DESC' ],
        ],
        where,
      })
    }
    else {
      professors = await Professor.findAll({
        order: [
          [ 'createdAt', 'DESC' ],
        ]
      })
    }
    res.send({
      response_status: 'SUCCESS',
      data: professors,
    })
  }
  catch (err) {
    res.send({
      response_status: 'ERROR',
      message: err.message || 'Some error occurred while retrieving the professors.',
    })
  }
}

// const findOne = async (req, res) => {
//   try {
//     const id = req.params.id
//     const professor = await Professor.findOne({ where: { id }})
//     res.send({
//       response_status: 'SUCCESS',
//       data: professor.dataValues,
//     })
//   }
//   catch (err) {
//     res.send({
//       response_status: 'ERROR',
//       message: err.message || 'Some error occurred while retrieving the professor.',
//     })
//   }
// }

// const update = async (req, res) => {
//   try {
//     const data = req.body
//     const id = req.params.id
//     const updatedProfessor = {
//       name: data.name,
//       price: data.price,
//       professor: data.professor,
//       type: data.type,
//     }
//     let professor = 0
//     if (data.imgUrl) {
//       let base64Image = data.imgUrl.split(';base64,').pop()
//       const imageBitMap = new Buffer(base64Image, 'base64')
//       let mime = await fileType.fromBuffer(imageBitMap)
//       const filePath = `public/professor-images/professor-${id}.${mime.ext}`
//       fs.writeFileSync(filePath, imageBitMap)

//       professor = await Professor.update({
//         ...updatedProfessor,
//         imgUrl: `https://bansaiservice.mediacreativecenter3.com/${filePath.replace('public/', '')}`,
//       },
//       {
//         where: { id }
//       })
//     }
//     else {
//       professor = await Professor.update(updatedProfessor, { where: { id }})
//     }
//     if (professor == 1) {
//       res.send({
//         response_status: 'SUCCESS',
//         message: 'Professor was updated successfully',
//       })
//     }
//     else {
//       res.send({
//         response_status: 'ERROR',
//         message: `Professor (ID: ${id}) was not found.`,
//       })
//     }
//   }
//   catch (err) {
//     res.send({
//       response_status: 'ERROR',
//       message: err.message || 'Some error occurred while updating the professor.',
//     })
//   }
// }

// const remove = async (req, res) => {
//   try {
//     const id = req.params.id
//     const professor = await Professor.destroy({ where: { id }})
//     if (professor == 1) {
//       res.send({
//         response_status: 'SUCCESS',
//         message: 'Professor was deleted successfully',
//       })
//     }
//     else {
//       res.send({
//         response_status: 'ERROR',
//         message: `Log (ID: ${id}) was not found.`,
//       })
//     }
//   }
//   catch (err) {
//     res.send({
//       response_status: 'ERROR',
//       message: err.message || 'Some error occurred while deleting the professor.',
//     })
//   }
// }

export default {
  create,
  findAll,
  // findOne,
  // update,
  // remove,
  // findAllWithTypes,
}