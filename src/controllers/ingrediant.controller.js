import db from '~/models'

const Ingrediant = db.Ingrediant
const Op = db.Sequelize.Op

const findAll = async(req, res) => {
    try {
        console.log('in')
        const ingrediant = await Ingrediant.findAll({
            order: [
                ['createdAt', 'DESC'],
            ]
        })

        res.send({
            response_status: 'SUCCESS',
            data: ingrediant,
        })
    } catch (err) {
        res.send({
            response_status: 'ERROR',
            message: err.message || 'Some error occurred while retrieving the weights.',
        })
    }
}

// const findOne = async (req, res) => {
//   try {
//     const id = req.params.id
//     const weight = await Weight.findOne({ where: { id }})
//     res.send({
//       response_status: 'SUCCESS',
//       data: weight.dataValues,
//     })
//   }
//   catch (err) {
//     res.send({
//       response_status: 'ERROR',
//       message: err.message || 'Some error occurred while retrieving the weight.',
//     })
//   }
// }

// const update = async (req, res) => {
//   try {
//     const data = req.body
//     const id = req.params.id
//     const updatedWeight = {
//       name: data.name,
//       price: data.price,
//       weight: data.weight,
//       type: data.type,
//     }
//     let weight = 0
//     if (data.imgUrl) {
//       let base64Image = data.imgUrl.split(';base64,').pop()
//       const imageBitMap = new Buffer(base64Image, 'base64')
//       let mime = await fileType.fromBuffer(imageBitMap)
//       const filePath = `public/weight-images/weight-${id}.${mime.ext}`
//       fs.writeFileSync(filePath, imageBitMap)

//       weight = await Weight.update({
//         ...updatedWeight,
//         imgUrl: `https://bansaiservice.mediacreativecenter3.com/${filePath.replace('public/', '')}`,
//       },
//       {
//         where: { id }
//       })
//     }
//     else {
//       weight = await Weight.update(updatedWeight, { where: { id }})
//     }
//     if (weight == 1) {
//       res.send({
//         response_status: 'SUCCESS',
//         message: 'Weight was updated successfully',
//       })
//     }
//     else {
//       res.send({
//         response_status: 'ERROR',
//         message: `Weight (ID: ${id}) was not found.`,
//       })
//     }
//   }
//   catch (err) {
//     res.send({
//       response_status: 'ERROR',
//       message: err.message || 'Some error occurred while updating the weight.',
//     })
//   }
// }

// const remove = async (req, res) => {
//   try {
//     const id = req.params.id
//     const weight = await Weight.destroy({ where: { id }})
//     if (weight == 1) {
//       res.send({
//         response_status: 'SUCCESS',
//         message: 'Weight was deleted successfully',
//       })
//     }
//     else {
//       res.send({
//         response_status: 'ERROR',
//         message: `Log (ID: ${id}) was not found.`,
//       })
//     }
//   }
//   catch (err) {
//     res.send({
//       response_status: 'ERROR',
//       message: err.message || 'Some error occurred while deleting the weight.',
//     })
//   }
// }

export default {
    findAll,
    // findOne,
    // update,
    // remove,
    // findAllWithTypes,
}