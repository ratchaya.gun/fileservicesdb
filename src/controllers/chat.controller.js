import db from '~/models'

const Chat = db.Chat
const Op = db.Sequelize.Op

const create = async(req, res) => {
    const data = req.body
    try {
        const resChat = await Chat.create(data)
        res.send({
            response_status: 'SUCCESS',
            data: {
                ...resChat.dataValues,
            },
        })
    } catch (err) {
        res.send({
            response_status: 'ERROR',
            message: err.message || 'Some error occurred while creating the chat.',
        })
    }
}

const findAll = async(req, res) => {
    try {
        let chats = []
        let lang = 'th'
        if (Object.keys(req.query).length > 0) {
            const where = {}
            Object.keys(req.query).forEach(key => {
                if (key === 'lang') {
                    lang = req.query[key]
                    return
                }
                where[key] = req.query[key]
            })
            chats = await Chat.findAll({
                order: [
                    ['createdAt', 'DESC'],
                ],
                where,
                include: ['user', 'professor'],
            })
        } else {
            chats = await Chat.findAll({
                order: [
                    ['createdAt', 'DESC'],
                ],
                include: ['user', 'professor'],
            })
        }
        res.send({
            response_status: 'SUCCESS',
            data: chats,
        })
    } catch (err) {
        res.send({
            response_status: 'ERROR',
            message: err.message || 'Some error occurred while retrieving the chats.',
        })
    }
}

// const findOne = async (req, res) => {
//   try {
//     const id = req.params.id
//     const chat = await Chat.findOne({ where: { id }})
//     res.send({
//       response_status: 'SUCCESS',
//       data: chat.dataValues,
//     })
//   }
//   catch (err) {
//     res.send({
//       response_status: 'ERROR',
//       message: err.message || 'Some error occurred while retrieving the chat.',
//     })
//   }
// }

// const update = async (req, res) => {
//   try {
//     const data = req.body
//     const id = req.params.id
//     const updatedChat = {
//       name: data.name,
//       price: data.price,
//       chat: data.chat,
//       type: data.type,
//     }
//     let chat = 0
//     if (data.imgUrl) {
//       let base64Image = data.imgUrl.split(';base64,').pop()
//       const imageBitMap = new Buffer(base64Image, 'base64')
//       let mime = await fileType.fromBuffer(imageBitMap)
//       const filePath = `public/chat-images/chat-${id}.${mime.ext}`
//       fs.writeFileSync(filePath, imageBitMap)

//       chat = await Chat.update({
//         ...updatedChat,
//         imgUrl: `https://bansaiservice.mediacreativecenter3.com/${filePath.replace('public/', '')}`,
//       },
//       {
//         where: { id }
//       })
//     }
//     else {
//       chat = await Chat.update(updatedChat, { where: { id }})
//     }
//     if (chat == 1) {
//       res.send({
//         response_status: 'SUCCESS',
//         message: 'Chat was updated successfully',
//       })
//     }
//     else {
//       res.send({
//         response_status: 'ERROR',
//         message: `Chat (ID: ${id}) was not found.`,
//       })
//     }
//   }
//   catch (err) {
//     res.send({
//       response_status: 'ERROR',
//       message: err.message || 'Some error occurred while updating the chat.',
//     })
//   }
// }

// const remove = async (req, res) => {
//   try {
//     const id = req.params.id
//     const chat = await Chat.destroy({ where: { id }})
//     if (chat == 1) {
//       res.send({
//         response_status: 'SUCCESS',
//         message: 'Chat was deleted successfully',
//       })
//     }
//     else {
//       res.send({
//         response_status: 'ERROR',
//         message: `Log (ID: ${id}) was not found.`,
//       })
//     }
//   }
//   catch (err) {
//     res.send({
//       response_status: 'ERROR',
//       message: err.message || 'Some error occurred while deleting the chat.',
//     })
//   }
// }

export default {
    create,
    findAll,
    // findOne,
    // update,
    // remove,
    // findAllWithTypes,
}