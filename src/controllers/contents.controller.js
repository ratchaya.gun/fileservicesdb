import db from '~/models'

const Contents = db.Contents
const Op = db.Sequelize.Op

const create = async (req, res) => {
  const data = req.body
  try {
    const resContents = await Contents.create({
      ...data,
      imgPath: '',
    })
    const contentsId = resContents.dataValues.id
    if (data.imgPath) {
      let base64Image = data.imgPath.split(';base64,').pop()
      const imageBitMap = new Buffer(base64Image, 'base64')
      let mime = await fileType.fromBuffer(imageBitMap)
      const filePath = `public/contents-images/contents-${contentsId}.${mime.ext}`
      fs.writeFileSync(filePath, imageBitMap)
      await resContents.update({
        imgPath: `/${filePath.replace('public/', '')}`,
      })
    }
    res.send({
      response_status: 'SUCCESS',
      data: {
        ...resContents.dataValues,
      },
    })
  }
  catch (err) {
    res.send({
      response_status: 'ERROR',
      message: err.message || 'Some error occurred while creating the contents.',
    })
  }
}

const findAll = async (req, res) => {
  try {
    let contents = []
    let lang = 'th'
    if (Object.keys(req.query).length > 0) {
      const where = {}
      Object.keys(req.query).forEach(key => {
        if (key === 'lang') {
          lang = req.query[key]
          return
        }
        where[key] = req.query[key]
      })
      contents = await Contents.findAll({
        order: [
          [ 'createdAt', 'DESC' ],
        ],
        where,
      })
    }
    else {
      contents = await Contents.findAll({
        order: [
          [ 'createdAt', 'DESC' ],
        ]
      })
    }
    res.send({
      response_status: 'SUCCESS',
      data: contents,
    })
  }
  catch (err) {
    res.send({
      response_status: 'ERROR',
      message: err.message || 'Some error occurred while retrieving the contents.',
    })
  }
}


export default {
  create,
  findAll,
  // findOne,
  // update,
  // remove,
  // findAllWithTypes,
}