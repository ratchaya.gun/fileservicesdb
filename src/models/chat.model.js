export default (sequelize, Sequelize) => {
    const Chat = sequelize.define('chat', {
        message: {
            type: Sequelize.STRING,
            defaultValue: '',
        },
        activeFlag: {
            type: Sequelize.BOOLEAN,
            defaultValue: true,
        },
        chatWithId: {
            type: Sequelize.INTEGER,
        }
    })
    return Chat
}