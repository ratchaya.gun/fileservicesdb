export default (sequelize, Sequelize) => {
  const Food = sequelize.define('food', {
    name: {
      type: Sequelize.STRING,
      required: true,
    },
    description: {
      type: Sequelize.STRING,
      defaultValue: '',
    },
    kcal: {
      type: Sequelize.DOUBLE,
      required: true,
    },
    type: {
      type: Sequelize.STRING,
      defaultValue: 'อื่นๆ',
    },
    imgPath: {
      type: Sequelize.STRING,
      defaultValue: null,
    },
    activeFlag: {
      type: Sequelize.BOOLEAN,
      defaultValue: true,
    }
  })
  return Food
}