import dbConfig from '~/config/db'
import Sequelize from 'sequelize'

import userModel from './user.model'
import weightModel from './weight.model'
import foodModel from './food.model'
import ingrediantModel from './ingrediant.model'
import mealModel from './meal.model'
import mealFoodModel from './mealFood.model'
import storeModel from './store.model'
import professorModel from './professor.model'
import chatModel from './chat.model'
import contentsModel from './contents.model'

const sequelize = new Sequelize(
  dbConfig.DB,
  dbConfig.USER,
  dbConfig.PASSWORD,
  {
    host: dbConfig.HOST,
    dialect: dbConfig.dialect,
    operatorsAliases: 0,
    pool: {
      max: dbConfig.pool.max,
      min: dbConfig.pool.min,
      acquire: dbConfig.pool.acquire,
      idle: dbConfig.idle,
    },
  },
)

const db = {}

db.Sequelize = Sequelize
db.sequelize = sequelize

db.User = userModel(sequelize, Sequelize)
db.Weight = weightModel(sequelize, Sequelize)
db.Food = foodModel(sequelize, Sequelize)
db.Ingrediant = ingrediantModel(sequelize, Sequelize)
db.Meal = mealModel(sequelize, Sequelize)
db.MealFood = mealFoodModel(sequelize, Sequelize)
db.Store = storeModel(sequelize, Sequelize)
db.Professor = professorModel(sequelize, Sequelize)
db.Chat = chatModel(sequelize, Sequelize)
db.Contents = contentsModel(sequelize, Sequelize)

db.User.hasMany(db.Weight)
db.Weight.belongsTo(db.User)

db.Food.hasMany(db.Ingrediant)
db.Ingrediant.belongsTo(db.Food)

//meal
db.User.hasMany(db.Meal)
db.Meal.belongsTo(db.User)

db.Meal.hasMany(db.MealFood)
db.MealFood.belongsTo(db.Meal)

db.Food.hasOne(db.MealFood)
db.MealFood.belongsTo(db.Food)

//store
db.Store.hasMany(db.Food)
db.Food.belongsTo(db.Store)

//chat
db.User.hasMany(db.Chat)
db.Chat.belongsTo(db.User)

db.Professor.hasMany(db.Chat)
db.Chat.belongsTo(db.Professor)
export default db