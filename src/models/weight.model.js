export default (sequelize, Sequelize) => {
  const Weight = sequelize.define('weight', {
    value: {
      type: Sequelize.DOUBLE,
      required: true,
    },
    date: {
      type: Sequelize.STRING,
      required: true,
    },
    activeFlag: {
      type: Sequelize.BOOLEAN,
      defaultValue: true,
    }
  })
  return Weight
}