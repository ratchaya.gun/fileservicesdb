export default (sequelize, Sequelize) => {
  const Professor = sequelize.define('professor', {
    name: {
      type: Sequelize.STRING,
      defaultValue: '',
    },
    bio: {
      type: Sequelize.STRING,
      defaultValue: '',
    },
    schedule: {
      type: Sequelize.JSON,
      defaultValue: [],
    },
    imgPath: {
      type: Sequelize.STRING,
    },
    activeFlag: {
      type: Sequelize.BOOLEAN,
      defaultValue: true,
    }
  })
  return Professor
}