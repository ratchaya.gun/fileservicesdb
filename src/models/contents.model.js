export default (sequelize, Sequelize) => {
  const Contents = sequelize.define('contents', {
    title: {
      type: Sequelize.STRING,
      defaultValue: '',
    },
    detail: {
      type: Sequelize.STRING,
      defaultValue: '',
    },
    imgPath: {
      type: Sequelize.STRING,
    },
    activeFlag: {
      type: Sequelize.BOOLEAN,
      defaultValue: true,
    }
  })
  return Contents
}