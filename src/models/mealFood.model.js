export default (sequelize, Sequelize) => {
    const MealFood = sequelize.define('mealFood', {
        activeFlag: {
            type: Sequelize.BOOLEAN,
            defaultValue: true,
        }
    })
    return MealFood
}