export default (sequelize, Sequelize) => {
  const Store = sequelize.define('store', {
    name: {
      type: Sequelize.STRING,
      defaultValue: '',
    },
    description: {
      type: Sequelize.STRING,
      defaultValue: '',
    },
    facebook: {
      type: Sequelize.STRING,
      defaultValue: '',
    },
    instagram: {
      type: Sequelize.STRING,
      defaultValue: '',
    },
    line: {
      type: Sequelize.STRING,
      defaultValue: '',
    },
    tel: {
      type: Sequelize.STRING,
      defaultValue: '',
    },
    foodsMenu: {
      type: Sequelize.JSON,
      defaultValue: [],
    },
    activeFlag: {
      type: Sequelize.BOOLEAN,
      defaultValue: true,
    }
  })
  return Store
}