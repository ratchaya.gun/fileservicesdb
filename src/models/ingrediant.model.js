export default (sequelize, Sequelize) => {
  const Ingrediant = sequelize.define('ingrediant', {
    name: {
      type: Sequelize.STRING,
      required: true,
    },
    description: {
      type: Sequelize.STRING,
      defaultValue: '',
    },
    amount: {
      type: Sequelize.STRING,
      required: true,
    },
    activeFlag: {
      type: Sequelize.BOOLEAN,
      defaultValue: true,
    }
  })
  return Ingrediant
}