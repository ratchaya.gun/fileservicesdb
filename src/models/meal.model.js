export default (sequelize, Sequelize) => {
  const Meal = sequelize.define('meal', {
    type: {
      type: Sequelize.STRING,
    },
    date: {
      type: Sequelize.STRING,
    },
    imgPath: {
      type: Sequelize.STRING,
    },
    activeFlag: {
      type: Sequelize.BOOLEAN,
      defaultValue: true,
    }
  })
  return Meal
}