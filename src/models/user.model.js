import bcrypt from 'bcrypt'

export default (sequelize, Sequelize) => {
    const User = sequelize.define('user', {
        username: {
            type: Sequelize.STRING,
            required: true,
            unique: true,
        },
        password: {
            type: Sequelize.STRING,
            required: true,
        },
        socialId: {
            type: Sequelize.STRING,
            required: true,
            unique: true,
        },
        gender: {
            type: Sequelize.ENUM('MALE', 'FEMALE', ''),
            defaultValue: '',
        },
        weight: {
            type: Sequelize.DOUBLE,
            defaultValue: 0,
        },
        goalWeight: {
            type: Sequelize.DOUBLE,
            defaultValue: 0,
        },
        height: {
            type: Sequelize.DOUBLE,
            defaultValue: 0,
        },
        age: {
            type: Sequelize.INTEGER,
            defaultValue: 0,
        },
        activityType: {
            type: Sequelize.INTEGER,
            defaultValue: 1,
        },
        bmr: {
            type: Sequelize.DOUBLE,
            defaultValue: 0,
        },
        allTargetKcal: {
            type: Sequelize.DOUBLE,
            defaultValue: 0,
        },
        dailyTargetKcal: {
            type: Sequelize.DOUBLE,
            defaultValue: 0,
        },
        tdee: {
            type: Sequelize.DOUBLE,
            defaultValue: 0,
        },
        targetDate: {
            type: Sequelize.INTEGER,
            defaultValue: 0,
        },
        allergy: {
            type: Sequelize.JSON,
            defaultValue: null,
        },
        activeFlag: {
            type: Sequelize.BOOLEAN,
            defaultValue: true,
        }
    }, {
        hooks: {
            beforeCreate: (user, options) => {
                {
                    user.password = user.password && user.password !== '' ? bcrypt.hashSync(user.password, 10) : ''
                }
            },
            beforeUpdate: (user, options) => {
                {
                    user.password = user.password && user.password !== '' ? bcrypt.hashSync(user.password, 10) : ''
                }
            },
        }
    })
    return User
}