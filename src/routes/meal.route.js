import mealController from '~/controllers/meal.controller'
import { Router } from 'express'

const router = Router()

router.get('/', mealController.findAll)
// router.get('/:id', mealController.findOne)
router.post('/', mealController.create)
// router.put('/:id', mealController.update)
router.delete('/:id', mealController.remove)

export default router