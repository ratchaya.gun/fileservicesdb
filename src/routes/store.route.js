import storeController from '~/controllers/store.controller'
import { Router } from 'express'

const router = Router()

router.get('/', storeController.findAll)
// router.get('/:id', storeController.findOne)
router.post('/', storeController.create)
// router.put('/:id', storeController.update)
// router.delete('/:id', storeController.remove)

export default router