import chatController from '~/controllers/chat.controller'
import { Router } from 'express'

const router = Router()

router.get('/', chatController.findAll)
// router.get('/:id', chatController.findOne)
router.post('/', chatController.create)
// router.put('/:id', chatController.update)
// router.delete('/:id', chatController.remove)

export default router