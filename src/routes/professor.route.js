import professorController from '~/controllers/professor.controller'
import { Router } from 'express'

const router = Router()

router.get('/', professorController.findAll)
// router.get('/:id', professorController.findOne)
router.post('/', professorController.create)
// router.put('/:id', professorController.update)
// router.delete('/:id', professorController.remove)

export default router