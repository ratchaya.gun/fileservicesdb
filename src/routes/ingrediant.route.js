import ingrediantController from '~/controllers/ingrediant.controller'
import { Router } from 'express'

const router = Router()

router.get('/', ingrediantController.findAll)

export default router