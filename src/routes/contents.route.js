import contentsController from '~/controllers/contents.controller'
import { Router } from 'express'

const router = Router()

router.get('/', contentsController.findAll)
// router.get('/:id', contentsController.findOne)
router.post('/', contentsController.create)
// router.put('/:id', contentsController.update)
// router.delete('/:id', contentsController.remove)

export default router