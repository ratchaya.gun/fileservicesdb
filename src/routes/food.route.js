import foodController from '~/controllers/food.controller'
import { Router } from 'express'

const router = Router()

router.get('/', foodController.findAll)
// router.get('/:id', foodController.findOne)
// router.post('/', foodController.create)
// router.put('/:id', foodController.update)
// router.delete('/:id', foodController.remove)

export default router