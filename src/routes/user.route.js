import userController from '~/controllers/user.controller'
import { Router } from 'express'

const router = Router()

router.get('/', userController.findAll)
router.get('/:id', userController.findOne)
router.post('/', userController.create)
router.post('/login', userController.login)
router.post('/loginWithSocial', userController.loginWithSocial)
router.put('/:id', userController.update)
router.delete('/:id', userController.remove)

export default router