import userRoutes from './user.route'
import weightRoutes from './weight.route'
import foodRoutes from './food.route'
import mealRoutes from './meal.route'
import storeRoutes from './store.route'
import professorRoutes from './professor.route'
import chatRoutes from './chat.route'
import ingrediantRoutes from './ingrediant.route'
import contentsRoutes from './contents.route'

export default app => {
    app.use('/users', userRoutes)
    app.use('/weights', weightRoutes)
    app.use('/foods', foodRoutes)
    app.use('/meals', mealRoutes)
    app.use('/stores', storeRoutes)
    app.use('/professors', professorRoutes)
    app.use('/chats', chatRoutes)
    app.use('/ingrediant', ingrediantRoutes)
    app.use('/contents', contentsRoutes)
}