import weightController from '~/controllers/weight.controller'
import { Router } from 'express'

const router = Router()

router.get('/', weightController.findAll)
// router.get('/:id', weightController.findOne)
router.post('/', weightController.create)
// router.put('/:id', weightController.update)
// router.delete('/:id', weightController.remove)

export default router